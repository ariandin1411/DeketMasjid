import { Component, NgZone, ElementRef, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation ,GeolocationOptions } from '@ionic-native/geolocation';
import { googlemaps } from 'googlemaps';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	@ViewChild('map') mapElement: ElementRef;

  map:any;
  latLng:any;
  markers:any;
  mapOptions:any;  
  isKM:any=2000;
  isType:any="";

  constructor(public navCtrl: NavController, private ngZone: NgZone, private geolocation : Geolocation) {

  }

  ionViewDidLoad() {
  	console.log('HomePage');
    this.loadMap();
  }

  loadMap(){

    this.geolocation.getCurrentPosition().then((position) => {
			
			this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			console.log('latLng',this.latLng);
     
      this.mapOptions = {
        center: this.latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    	}   

			this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

    }, (err) => {
      alert('err '+err);
    });

  }

  nearbyPlace(){

    this.loadMap();
    this.markers = [];
    let service = new google.maps.places.PlacesService(this.map);
    service.nearbySearch({
              location: this.latLng,
              radius: this.isKM,
              types: [this.isType]
            }, (results, status) => {
                this.callback(results, status);
            });
  }

  callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      for (var i = 0; i < results.length; i++) {
      	console.log(results[i]);
        this.createMarker(results[i]);
      }
    }
  }

  createMarker(place){
    var placeLoc = place;
    console.log('placeLoc',placeLoc);
    // this.markers = new google.maps.Marker({
    //     map: this.map,
    //     position: place.geometry.location
    // });

    let newmarkers = new google.maps.Marker({
        map: this.map,
        position: place.geometry.location,
        icon: 'assets/icon/mosquee.png'
    });

    let infowindow = new google.maps.InfoWindow();

    google.maps.event.addListener(newmarkers, 'click', () => {
      this.ngZone.run(() => {
        infowindow.setContent(place.name);
        infowindow.open(this.map, newmarkers);
      });
    });
  }

}
